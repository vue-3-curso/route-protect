import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutViewVue from '@/views/AboutView.vue'
import LoginView from '@/views/LoginView.vue'
import DashBoardView from '@/views/DashboardView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta:{
      requireAuth:false
    }
  },
  {
    path: '/about',
    name: 'about',
    component: AboutViewVue,
    meta:{
      requireAuth:false
    }
  }
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta:{
      requireAuth:false
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DashBoardView,
    meta:{
      requireAuth:true
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
  /* depende donde se guarda el token/sesion, controlamos que el usuario esté autenticado
    const auth=getAuth().currentUser !=null
    Para el ejemplo se usa false
  */
  const auth=false
  const needAuth=to.meta.requireAuth

  if(needAuth && !auth){
    next('login')
  }else{
    //next vacío indica a la ruta que quiere proceder
    next()
  }

})

export default router
